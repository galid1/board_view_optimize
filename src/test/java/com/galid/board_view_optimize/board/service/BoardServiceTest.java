package com.galid.board_view_optimize.board.service;

import com.galid.board_view_optimize.board.domain.Board;
import com.galid.board_view_optimize.board.domain.BoardRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.persistence.EntityManager;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class BoardServiceTest {
    private int THREAD_CNT = 100;
    private ExecutorService ex = Executors.newFixedThreadPool(THREAD_CNT);
    private CountDownLatch latch = new CountDownLatch(THREAD_CNT);

    @Autowired
    private BoardService boardService;
    @Autowired
    private BoardRepository repository;

    @Test
    public void 동시성_테스트() throws Exception {
        // given
        Long boardId = boardService.write(new WriteBoardRequest("TEST", "TEST"));

        // when
        for (int i = 0; i < 100; i ++) {
            ex.execute(() -> {
                boardService.read(boardId);
                latch.countDown();
            });
        }
        latch.await();

        // then
        Board board = repository.findById(boardId).get();
        assertThat(board.getViewCount(), is(100));
    }

}