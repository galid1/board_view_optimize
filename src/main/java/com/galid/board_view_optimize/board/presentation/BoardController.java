package com.galid.board_view_optimize.board.presentation;

import com.galid.board_view_optimize.board.service.BoardDetail;
import com.galid.board_view_optimize.board.service.BoardService;
import com.galid.board_view_optimize.board.service.WriteBoardRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class BoardController {
    private final BoardService boardService;

    @GetMapping("/write")
    public void write() {
        boardService.write(new WriteBoardRequest("hi", "ok"));
    }

    @GetMapping("/board/{boardId}")
    public BoardDetail read(@PathVariable Long boardId) {
        BoardDetail read = boardService.read(boardId);
        return read;
    }
}
