package com.galid.board_view_optimize.board.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OptimisticLocking;

import javax.persistence.*;

@Entity
@Table(name = "board")
@Getter
@NoArgsConstructor
public class Board {
    @Id @GeneratedValue
    private Long boardId;

    private String title;
    private String content;

    private int hit;

    public Board(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public void increaseViewCount(int hit) {
        this.hit += hit;
    }
}
