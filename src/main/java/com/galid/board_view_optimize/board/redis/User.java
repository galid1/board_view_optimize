package com.galid.board_view_optimize.board.redis;

import lombok.Getter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;

@RedisHash("student")
@Getter
@ToString
public class User implements Serializable {
    @Id
    private String userId;
    private String name;
    private int count;

    public User(String userId, String name) {
        this.userId = userId;
        this.name = name;
    }

    public void increase() {
        this.count += 1;
    }
}
