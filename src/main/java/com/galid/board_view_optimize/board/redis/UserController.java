package com.galid.board_view_optimize.board.redis;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.SplittableRandom;

@RestController
@RequiredArgsConstructor
@Slf4j
public class UserController {
    private final UserRedisRepository repository;

    @GetMapping("/ok")
    public String ok() {
        return "ok";
    }

    @GetMapping("/save")
    public String save() {
        User user = new User("1", "Test");
        log.info("save user={}", user);
        repository.save(user);
        return "save";
    }

    @GetMapping("/get")
    public User get() {
        return repository.findById("1").get();
    }

    @GetMapping("/incre")
    public void incre() {
        User user = repository.findById("1")
                .orElseThrow();
        user.increase();
        repository.save(user);
    }

    private String createId() {
        SplittableRandom random = new SplittableRandom();
        return String.valueOf(random.nextInt(1, 1_000_000_000));
    }
}
