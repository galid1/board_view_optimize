package com.galid.board_view_optimize.board.service;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class BoardDetail {
    private Long boardId;
    private String title;
    private String content;
    private Integer viewCount;
}
