package com.galid.board_view_optimize.board.service;

import com.galid.board_view_optimize.board.domain.Board;
import com.galid.board_view_optimize.board.domain.BoardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Transactional
@RequiredArgsConstructor
@Service
public class BoardService {
    private final BoardRepository boardRepository;
    private int tempHit;

    public Long write(WriteBoardRequest request) {
        Board board = new Board(request.getTitle(), request.getContent());
        Board savedBoard = boardRepository.save(board);

        return savedBoard.getBoardId();
    }

    public BoardDetail read(Long boardId) {
        Board board = boardRepository.findById(boardId)
                .get();

        tempHit += 1;

        if (tempHit > 10000) {
            board.increaseViewCount(tempHit);
            tempHit = 0;
        }

        return new BoardDetail(board.getBoardId(), board.getTitle(), board.getContent(), board.getHit());
    }

    public List<BoardDetail> list() {
        return boardRepository.findAll().stream()
                .map(b -> new BoardDetail(b.getBoardId(), b.getTitle(), b.getContent(), b.getHit()))
                .collect(Collectors.toList());
    }
}
