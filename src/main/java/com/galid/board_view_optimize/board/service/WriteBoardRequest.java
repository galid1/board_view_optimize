package com.galid.board_view_optimize.board.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
@AllArgsConstructor
public class WriteBoardRequest {
    private String title;
    private String content;
}
