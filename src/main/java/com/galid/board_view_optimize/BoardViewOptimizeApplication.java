package com.galid.board_view_optimize;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BoardViewOptimizeApplication {

    public static void main(String[] args) {
        SpringApplication.run(BoardViewOptimizeApplication.class, args);
    }

}
